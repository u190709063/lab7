public class MyDate {
    int day, month, year;
    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public String toString() {
        if (month > 9 || day > 9) {
            if (day > 9) {
                if(month <= 9)
                    return year + "-0" + month + "-" + day;
                else
                    return year + "-" + month + "-" + day;
            } else {
                return year + "-" + month + "-0" + day;
            }
        }
        return year + "-0" + month + "-0" + day;
    }
    void incrementYear() {
        if ((year % 400 != 0) && (((year % 4 != 0) || (year % 100 == 0))
                || month != 2 || day != 29)) {
        } else {
            day = 28;
        }
        year++;
    }
    void incrementYear(int i) {
        if ((year % 400 != 0) && (((year % 4 != 0) || (year % 100 == 0))
                || month != 2 || day != 29 || i % 4 == 0)) {
        } else {
            day = 28;
        }
        year = year + i;
    }
    void decrementYear() {
        if ((year % 400 != 0) && (((year % 4 != 0) || (year % 100 == 0))
                || month != 2 || day != 29)) {
        } else {
            day = 28;
        }
        year--;
    }
    void decrementYear(int y) {
        if ((year % 400 != 0) && (((year % 4 != 0) || (year % 100 == 0))
                || month != 2 || day != 29 || y % 4 == 0)) {
        } else {
            day = 28;
        }
        year = year - y;
    }
    void incrementMonth() {
        if (month != 12) {
            if (month==1 && (day==31 || day==30 || day==29)) {
                month++;
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                    day=29;
                else
                    day=28;
            }
            else if (month!=7 && day==31) {
                month++;
                day=30;
                if (month==1 || month==3 || month==5 || month==7
                        || month==8 || month==10 || month==12)
                    day=31;
            }
            else
                month++;
        } else {
            month = 1;
            incrementYear();
        }
    }
    void incrementMonth(int m) {
        month = month + m;
        if (month <= 12) {
        } else {
            incrementYear(month / 12);
            month = month % 12;
        }
        if (day != 31 || (month != 4 && month != 6 && month != 9 && month != 11)) {
            if (month==2 && (day==31 || day==30 || day==29)) {
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                    day=29;
                else
                    day=28;
            }
        } else {
            day=30;
        }
    }
    void decrementMonth() {
        if (month != 1) {
            if (month==3 && (day==31 || day==30 || day==29)) {
                month--;
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                    day=29;
                else
                    day=28;
            }
            else if (month!=8 && day==31) {
                month--;
                day=30;
                if (month==1 || month==3 || month==5 || month==7
                        || month==8 || month==10 || month==12)
                    day=31;
            }
            else
                month--;
        } else {
            month = 12;
            decrementYear();
        }
    }
    void decrementMonth(int d) {
        month = month - d;
        if (month <= 0) {
            decrementYear((12 - month) / 12);
            month = Math.abs(12 + month) % 12 ;
        }
        if (day==31 && (month==4 || month==6 || month==9 || month==11))
            day=30;
        else if (month==2 && (day==31 || day==30 || day==29)) {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                day=29;
            else
                day=28;
        }
    }
    void incrementDay() {
        day = day + 1;
        if (day == 32 || (day == 31 && (month == 4 || month == 6 || month == 9 || month == 11)) || ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) && day == 30 && month == 2) {
            day = 1;
            incrementMonth();
        } else if (month == 2 && day == 29) {
            day = 1;
            incrementMonth();
        }
    }
    void incrementDay(int i1) {
        for (int i=0; i<i1; i++)
            incrementDay();
    }
    void decrementDay() {
        day--;
        if(day==0) {
            day = 31;
            decrementMonth();
        }
    }
    void decrementDay(int d1) {
        for (int i = d1 - 1; i >= 0; i--)
            decrementDay();
    }
    boolean isBefore(MyDate secondDate) {
        if (year < secondDate.year || month < secondDate.month || day < secondDate.day) return true;
        else return false;
    }
    boolean isAfter(MyDate secondDate) {
        if (year > secondDate.year || month > secondDate.month || day > secondDate.day) return true;
        else return false;
    }
    int dayDifference(MyDate secondDate) {
        int sum = 0;
        int sum2 = 0;
        int control = 0;
        if (year == secondDate.year) {
            {
                int i=0;
                while (i<month) {
                    if (i==4 || i==6 || i==9 || i==11)
                        sum += 30;
                    else if (i==1 || i==3 || i==5 || i==7 || i==8 || i==10 || i==12)
                        sum += 31;
                    else if (i==2 && ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))))
                        sum += 29;
                    else
                        sum += 28;
                    i++;
                }
            }
            int i=0;
            while (i< secondDate.month) {
                if (i==4 || i==6 || i==9 || i==11)
                    sum2 += 30;
                else if (i==1 || i==3 || i==5 || i==7 || i==8 || i==10 || i==12)
                    sum2 += 31;
                else if (i==2 && ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))))
                    sum2 += 29;
                else
                    sum2 += 28;
                i++;
            }
        }
        else
        
        if (year < secondDate.year) {
            control = year;
            int i=0;
            while (i < secondDate.year - year) {
                if (((control % 400 == 0) || ((control % 4 == 0) && (control % 100 != 0))))
                    sum += 366;
                else
                    sum += 365;
                control++;
                i++;
            }
        }
        else
            control = secondDate.year;

        //absolute value=Math.abs
        //abs(day+sum)-(date.day+sumx)
        
        int i = 0;
        while (i < year - secondDate.year) {
            if (((control % 400 != 0) && ((control % 4 != 0) || (control % 100 == 0)))) {
                sum2 += 365;
            } else {
                sum2 += 366;
            }
            control++;
            i++;
        }
        int x = Math.abs((sum + day) - (sum2 + secondDate.day));
        return x;
    }
}

